# Setup
Firstly, install the necessary project dependencies:
```shell
$ yarn install
```

You can then run the application on webpack dev server and start working:
```shell
$ yarn run dev-server
```

Dev server does not build a physical bundle.js file. If you want to build the bundle for use outside of webpack dev server, run the following command:
```shell
$ yarn run build
```

# Usage
Indecision is the first of two simple `React`  based web applications built while following  [_The Complete React Web Developer Course_](https://www.udemy.com/react-2nd-edition/) by [_Andrew Mead_](https://mead.io) on [Udemy](https://udemy.com).