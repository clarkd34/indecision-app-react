// const square = function(x) {
//     return x * x;
// };

// const squareArrow = (x) => x * x;

// console.log(squareArrow(8));

const getFirstName = (x) => {
    return x.split(' ')[0]
};

const getFirstNameTwo = (x) => x.split(' ')[0];

console.log(getFirstName('Declan Clarke'));
console.log(getFirstNameTwo('Mike Smith'))