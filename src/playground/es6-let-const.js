var nameVar = 'Declan';
var nameVar = 'Mike';
console.log('nameVar', nameVar);

let nameLet = 'John';
nameLet = 'Jane';
console.log('nameLet', nameLet);

const nameConst = 'Frank';
console.log('nameConst', nameConst);

const fullName = 'Declan Clarke';
let firstName;
if(fullName) {
    firstName = fullName.split(' ')[0];
    console.log('firstName', firstName);
}

console.log('firstName', firstName);